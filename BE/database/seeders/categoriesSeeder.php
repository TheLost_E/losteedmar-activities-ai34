<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class categoriesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('categories')->insert([
            'category' =>'Laravel',
        ]);
        DB::table('categories')->insert([
            'category' =>'Vue',
        ]);
        DB::table('categories')->insert([
            'category' =>'PHP',
        ]);

    }
}
