<?php
use App\Controllers\booksController;
use App\Controllers\borrowedbooksController;
use App\Controllers\returnedbooksController;
use App\Controllers\categoriesController;
use App\Controllers\patronsController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('books', booksController::class);
Route::resource('borrowed_books', borrowedbooksController::class);
Route::resource('returned_books', returnebooksController::class);
Route::resource('categories', categoriesController::class);
Route::resource('patrons', patronsController::class);
