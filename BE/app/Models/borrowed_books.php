<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class borrowed_books extends Model
{
    use HasFactory;

    protected $table = 'borrowed_books';
    protected $fillable = ['patron_id', 'copies', 'book_id'];
    
    public function books()
    {
        return $this->belongsTo(books::class, 'book_id', 'id');
    }
    public function patrons()
    {
        return $this->belongsTo(patrons::class, 'patron_id', 'id');
    }
}
