<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class patrons extends Model
{
    use HasFactory;

    protected $table = 'patrons';
    protected $fillable = ['last_name', 'first_name', 'middle_name', 'email'];

    public function borrowed_books()
    {
        return $this->hasMany(borrowed_books::class, 'borrowed_books', 'patron_id');
    }
    public function returned_books()
    {
        return $this->hasMany(returned_books::class, 'returned_books', 'patron_id');
    }
}
