<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class returned_books extends Model
{
    use HasFactory;

    protected $table = 'returned_books';
    protected $fillable = ['patron_id', 'copies', 'book_id'];

    public function books()
    {
        return $this->belongsTo(books::class, 'book_id', 'id');
    }
    public function patrons()
    {
        return $this->belongsTo(patrons::class, 'patron_id', 'id');
    }
}
