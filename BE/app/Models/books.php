<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class books extends Model
{
    use HasFactory;

    protected $table = 'books';
    protected $fillable = ['name', 'author', 'copies', 'category_id'];

    public function categories()
    {
        return $this->belongsTo(categories::class);
    }
    public function borrowed_books()
    {
        return $this->hasMany(borrowed_books::class, 'borrowed_books', 'book_id');

    }
    public function returned_books()
    {
        return $this->hasMany(returned_books::class, 'returned_books', 'book_id');

    }
}
