<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class returnedbookrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
            return [
                'book_id' => ['required', 'numeric'],
                'copies' => ['required', 'numeric|min:2'],
                'patron_id' => ['required', 'numeric'],
            ];
    }
    public function messages()
{
    return [
        'book_id.required' => 'Book ID is required',
        'copies.required' => 'Capies is required',
        'patron_id.required' => 'Patron ID is required',
    ];
}
}
