<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class booksRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => ['required', 'string'],
            'author' => ['required', 'string'],
            'copies' => ['required', 'integer'],
            'category_id' => ['required', 'integer'],
        ];
    }
    public function messages()
{
    return [
        'name.required' => 'Name is required',
        'author.required' => 'Author is required',
        'copies.required' => 'Capies is required',
        'category_id.required' => 'Category ID is required',
    ];
}
}
