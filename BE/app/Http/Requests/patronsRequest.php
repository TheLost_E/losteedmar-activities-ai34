<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class patronsRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'last_name' => ['required', 'string'],
            'fisrt_name' => ['required', 'string'],
            'middle_name' => ['required', 'string'],
            'email' => ['required', 'email'],
        ];
    }
    public function messages()
    {
    return [
        'last_name.required' => 'Last Name is required',
        'first_name.required' => 'First Name is required',
        'middle_name.required' => 'Middle Name is required',
        'email.required' => 'Email is required',
    ];
}
}
