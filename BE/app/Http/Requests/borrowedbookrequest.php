<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class borrowedbookrequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'patron_id' => ['required', 'numeric'],
            'copies' => ['required', 'numeric|max:2'],
            'book_id' => ['required', 'numeric'],
        ];
    }
    public function messages()
{
    return [
        'patron_id.required' => 'Patron ID is required',
        'copies.required' => 'Capies is required',
        'book_id.required' => 'Book ID is required',
    ];
}
}
