<?php

namespace App\Http\Controllers;
use App\Models\returned_books;
use Illuminate\Http\Request;

class returnedbooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $returned_books = returned_books::all();
        return response()->json($returned_books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json($returned_books);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(returnedbooksrequest $request)
    {
        $this->validate($request, [
            'book_id' => 'required|numeric',
            'copies' => 'required|numeric',
            'patron_id' => 'required|numeric',
        ]);

        $input = $request->validate();
        returned_books::create($input);
        return response()->json($returned_books);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $returned_books = returned_books::findOrFail($id);
        return response()->json($returned_books);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $returned_books = returned_books::find($id);
        return response()->json($returned_books);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(returnedbooksrequest $request, $id)
    {
        $returned_books = returned_books::findOrFail($id);

        $this->validate($request, [
            'last_name' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'email' => 'required|email',
        ]);

        $input = $request->validate(); 
        $returned_books->fill($input)->save();
        return response()->json($returned_books);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $returned_books = returned_books::findOrFail($id);
        $returned_books->delete();
        return response()->json($returned_books);
    }
}
