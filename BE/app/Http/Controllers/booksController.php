<?php

namespace App\Http\Controllers;

use App\Models\books;
use Illuminate\Http\Request;

class booksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $books = books::all();
        return response()->json($books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json($books);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(booksRequest $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'author' => 'required',
            'copies' => 'required|numeric',
            'category_id' => 'required|numeric',
        ]);

        $input = $request->validate();
        books::create($input);
        return response()->json($books);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $books = books::findOrFail($id);
        return response()->json($books);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $books = books::find($id);
        return response()->json($books);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(booksRequest $request, $id)
    {
        $books = books::findOrFail($id);

        $this->validate($request, [
            'name' => 'required',
            'author' => 'required',
            'copies' => 'required|numeric',
            'category_id' => 'required|numeric',
        ]);

        $input = $request->validate(); 
        $books->fill($input)->save();
        return response()->json($books);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $books = books::findOrFail($id);
        $books->delete();
        return response()->json($books);
    }
}
