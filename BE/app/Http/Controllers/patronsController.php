<?php

namespace App\Http\Controllers;
use App\Models\patrons;
use Illuminate\Http\Request;

class patronsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $patrons = patrons::all();
        return response()->json($patrons);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json($patrons);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(patronsRequest $request)
    {
        $this->validate($request, [
            'last_name' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'email' => 'required|email',
        ]);

        $input = $request->validate();
        patrons::create($input);
        return response()->json($patrons);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $patrons = patrons::findOrFail($id);
        return response()->json($patrons);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $patrons = patrons::find($id);
        return response()->json($patrons);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(patronsRequest $request, $id)
    {
        $patrons = patrons::findOrFail($id);

        $this->validate($request, [
            'last_name' => 'required',
            'first_name' => 'required',
            'middle_name' => 'required',
            'email' => 'required|email',
        ]);

        $input = $request->validate(); 
        $patrons->fill($input)->save();
        return response()->json($patrons);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $patrons = patrons::findOrFail($id);
        $patrons->delete();
        return response()->json($patrons);
    }
}
