<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Http\Requests\bookRequest;
use App\Http\Requests\borrowedbookrequest;
use App\Http\Requests\categoriesRequest;
use App\Http\Requests\returnedbookrequest;
use App\Http\Requests\patronsRequest;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;
}
