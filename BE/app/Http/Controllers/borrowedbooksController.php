<?php

namespace App\Http\Controllers;
use App\Models\borrowed_books;
use Illuminate\Http\Request;

class borrowedbooksController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $borrowed_books = borrowed_books::all();
        return response()->json($borrowed_books);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return response()->json($borrowed_books);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(borrowedbooksrequest $request)
    {
        $this->validate($request, [
            'patron_id' => 'required|numeric',
            'copies' => 'required|numeric',
            'book_id' => 'required|numeric',
        ]);

        $input = $request->validate();
        borrowed_books::create($input);
        return response()->json($borrowed_books);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $borrowed_books = borrowed_books::findOrFail($id);
        return response()->json($borrowed_books);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $borrowed_books = borrowed_books::find($id);
        return response()->json($borrowed_books);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(borrowedbooksrequest $request, $id)
    {
        $borrowed_books = borrowed_books::findOrFail($id);

        $this->validate($request, [
            'patron_id' => 'required|numeric',
            'copies' => 'required|numeric',
            'book_id' => 'required|numeric',
        ]);

        $input = $request->validate(); 
        $borrowed_books->fill($input)->save();
        return response()->json($borrowed_books);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $borrowed_books = borrowed_books::findOrFail($id);
        $borrowed_books->delete();
        return response()->json($borrowed_books);
    }
}
