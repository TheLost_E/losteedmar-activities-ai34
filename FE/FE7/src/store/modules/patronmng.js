import axios from "axios";

const state = {
  patronmng: [],
};

const getters = {
  Allpatronmng: (state) => state.patronmng,
};

    const actions = {
    async fetchpatronmng({ commit }) 
    {
    const response = await axios.get("http://127.0.0.1:8000/api/patronmng");
    commit("Patrons", response.data.data);
    },
    async addpatronmng({ commit }, patronmng) 
    {
    const response = await axios.post(
      "http://127.0.0.1:8000/api/patronmng", patronmng);
    commit("NewPatron", response.data.data);
    },
    async updatepatronmng({ commit }, patronmng) 
    {
    const response = await axios.put(
      `http://127.0.0.1:8000/api/patronmng/${patronmng.id}`,patronmng);
    commit("EditPatron", response.data);
    },
    async removepatronmng({ commit }, patronmng) 
    {
    axios.delete(`http://127.0.0.1:8000/api/patronmng/${patronmng.id}`, patronmng);
    commit("DeletePatron", patronmng);
    },
};

const mutations = {
  Patrons: (state, patronmng) => (state.patronmng = patronmng),
  NewPatron: (state, patronmng) => state.patronmng.unshift(patronmng),
  EditPatron: (state, patronmng) => {
    const index = state.patronmng.findIndex((t) => t.id === patronmng.id);
    if (index !== -1) {
      state.patronmng.splice(index, 1, patronmng);
    }
  },
  DeletePatron: (state, patronmng) =>
    (state.patronmng = state.patronmng.filter((t) => patronmng.id !== t.id)),
};

export default {
  state,
  getters,
  actions,
  mutations,
};
