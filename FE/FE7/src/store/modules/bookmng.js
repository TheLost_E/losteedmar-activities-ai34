import axios from "axios";

const state = {
  bookmng: [],
};

const getters = {
  Allbookmng: (state) => state.bookmng,
};

    const actions = {
    async fetchbookmng({ commit }) 
    {
    const response = await axios.get("http://127.0.0.1:8000/api/bookmng");
    commit("Books", response.data.data);
    },
    async addbookmng({ commit }, bookmng) 
    {
    const response = await axios.post(
      "http://127.0.0.1:8000/api/bookmng", bookmng);
    commit("NewBooks", response.data.data);
    },
    async updatebookmng({ commit }, bookmng) 
    {
    const response = await axios.put(
      `http://127.0.0.1:8000/api/bookmng/${bookmng.id}`,bookmng);
    commit("EditBooks", response.data);
    },
    async removebookmng({ commit }, bookmng) 
    {
    axios.delete(`http://127.0.0.1:8000/api/bookmng/${bookmng.id}`, bookmng);
    commit("DeleteBooks", bookmng);
    },
};

const mutations = {
  Books: (state, bookmng) => (state.bookmng = bookmng),
  NewBooks: (state, bookmng) => state.bookmng.unshift(bookmng),
  EditBooks: (state, bookmng) => {
    const index = state.bookmng.findIndex((t) => t.id === bookmng.id);
    if (index !== -1) {
      state.bookmng.splice(index, 1, bookmng);
    }
  },
  DeletePatron: (state, bookmng) =>
    (state.bookmng = state.bookmng.filter((t) => bookmng.id !== t.id)),
};

export default {
  state,
  getters,
  actions,
  mutations,
};