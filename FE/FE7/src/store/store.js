import Vue from "vue";
import Vuex from "vuex";
import bookmng from "./modules/bookmng";
import patronmng from "./modules/patronmng";
//Create store

//load Vuex
Vue.use(Vuex);

export default new Vuex.Store({
  modules: [bookmng, patronmng],
});
